package cz.hel.randomfiller;


/**
 *
 * @author j
 */
public interface Filler {

	<E> E fillDummy(Class<E> clazz, GenerationRule... rules);

}
