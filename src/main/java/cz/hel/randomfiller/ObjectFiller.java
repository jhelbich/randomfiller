package cz.hel.randomfiller;

import cz.hel.randomfiller.core.cache.GenerationRulesCache;
import cz.hel.randomfiller.core.impl.DummyFiller;

/**
 *
 * @author j
 */
public class ObjectFiller implements Filler {

	@Override
	public <E> E fillDummy(Class<E> clazz, GenerationRule... rules) {
		checkWrongStructures(clazz);
		GenerationRulesCache.getInstance().createRulesCache(rules);
		return new DummyFiller().generate(clazz);
	}

	// TODO primitives and enumerations should work
	private <E> void checkWrongStructures(Class<E> clazz) {
		if (clazz.isAnnotation() || clazz.isEnum() || clazz.isInterface()
				|| clazz.isPrimitive() || clazz.isLocalClass()) {
			throw new IllegalArgumentException(
					"Wrong structure provided. Provide a simple java class object.");
		}
	}
}
