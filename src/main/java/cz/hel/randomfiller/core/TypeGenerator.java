package cz.hel.randomfiller.core;

/**
 *
 * @author j
 */
public interface TypeGenerator {

	/**
	 * Returns one object of generated type, e.g. "abc" for String or List<?>
	 * for collections.
	 *
	 * @param clazz
	 * @return Object
	 */
	Object getGeneratedObject(Class<?> clazz);
}
