package cz.hel.randomfiller.core.cache;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import cz.hel.randomfiller.GenerationRule;

/**
 *
 * @author j
 */
public class GenerationRulesCache {

	private static GenerationRulesCache INSTANCE;

	private Set<GenerationRule> rulesCache;

	private GenerationRulesCache() {
		rulesCache = new HashSet<>();
	}

	public static GenerationRulesCache getInstance() {
		if (INSTANCE == null) {
			synchronized (GenerationRulesCache.class) {
				if (INSTANCE == null) {
					INSTANCE = new GenerationRulesCache();
				}
			}
		}
		return INSTANCE;
	}

	public Set<GenerationRule> getRulesCache() {
		return rulesCache;
	}

	public void createRulesCache(GenerationRule[] rules) {
		rulesCache.clear();
		if (rules != null) {
			rulesCache.addAll(Arrays.asList(rules));
		}
	}

}
