package cz.hel.randomfiller.core.impl;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Id;

import cz.hel.randomfiller.core.impl.primitive.ConstraintHandler;
import cz.hel.randomfiller.core.impl.primitive.IdGenerator;
import cz.hel.randomfiller.core.impl.primitive.LengthGenerator;

/**
 *
 * @author j
 */
public class AnnotationConstraintsManager {

	private static AnnotationConstraintsManager instance = new AnnotationConstraintsManager();
	private Map<String, ConstraintHandler> generators;
	private Map<String, Object> constraints;

	// TODO what?
	private AnnotationConstraintsManager() {
		generators = new HashMap<>();
		constraints = new HashMap<>();
		generators.put("length", new LengthGenerator());
		generators.put("id", new IdGenerator());
	}

	private void addConstraint(Id id) {
		constraints.put("id", null);
	}

	private void addConstraint(Column c) {
		constraints.put("nullable", c.nullable());
		constraints.put("unique", c.unique());
		constraints.put("length", c.length());
		constraints.put("precision", c.precision());
		constraints.put("scale", c.scale());
		constraints.put("name", c.name());
		constraints.put("table", c.table());
		constraints.put("columndefinition", c.columnDefinition());
		constraints.put("updatable", c.updatable());
		constraints.put("insertable", c.insertable());
	}

	// called once for each field
	public void addConstraintVar(Annotation[] annotations) {
		constraints.clear();
		if (annotations == null || annotations.length == 0) {
			return;
		}
		for (Annotation a : annotations) {
			if (a.annotationType().equals(Column.class)) {
				addConstraint((Column) a);
			} else if (a.annotationType().equals(Id.class)) {
				addConstraint((Id) a);
			}
		}
	}

	public Object handleObjectConstraints(Object actual) {
		for (Map.Entry<String, Object> c : constraints.entrySet()) {
			ConstraintHandler handler = generators.get(c.getKey());
			if (handler != null) {
				actual = handler.generateValue(actual, c.getValue());
			}
		}
		return actual;
	}

	public Map<String, Object> getConstraints() {
		return constraints;
	}

	public static AnnotationConstraintsManager getInstance() {
		return instance;
	}
}
