package cz.hel.randomfiller.core.impl;

import java.util.ArrayList;
import java.util.Arrays;

import cz.hel.randomfiller.GenerationRule;
import cz.hel.randomfiller.core.TypeGenerator;
import cz.hel.randomfiller.core.cache.GenerationRulesCache;

/**
 *
 * @author j
 */
public class CollectionGenerator implements TypeGenerator {

	@Override
	public Object getGeneratedObject(Class<?> clazz) {
		if (GenerationRulesCache.getInstance().getRulesCache()
				.contains(GenerationRule.NO_COLLECTIONS)) {
			return new ArrayList<>();
		}
		if (clazz.isPrimitive()
				|| clazz.getPackage().getName().startsWith("java.lang")) {
			TypeGenerator pg = new PrimitiveGenerator();
			return Arrays.asList(pg.getGeneratedObject(clazz));
		} else {
			// TODO jenom se modlim, at to neni interface atd.
			TypeGenerator cg = new ComplexGenerator();
			return Arrays.asList(cg.getGeneratedObject(clazz));
		}
	}
}
