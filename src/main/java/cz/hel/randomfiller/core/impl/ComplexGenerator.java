package cz.hel.randomfiller.core.impl;

import java.util.Set;

import cz.hel.randomfiller.Filler;
import cz.hel.randomfiller.GenerationRule;
import cz.hel.randomfiller.ObjectFiller;
import cz.hel.randomfiller.core.TypeGenerator;
import cz.hel.randomfiller.core.cache.GenerationRulesCache;

/**
 *
 * @author j
 */
public class ComplexGenerator implements TypeGenerator {

	@Override
	public Object getGeneratedObject(Class<?> clazz) {
		Set<GenerationRule> rulesCache = GenerationRulesCache.getInstance()
				.getRulesCache();

		// do not generate another complex object if rules specify so
		if (!rulesCache.contains(GenerationRule.NO_COPLEX_TYPES)) {
			Filler filler = new ObjectFiller();
			GenerationRule[] rules = rulesCache.toArray(new GenerationRule[] {});
			return filler.fillDummy(clazz, rules);
		}
		return null;
	}
}
