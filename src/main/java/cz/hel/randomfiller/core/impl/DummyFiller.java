package cz.hel.randomfiller.core.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author j
 */
public class DummyFiller {

	public <E> E generate(Class<E> clazz) {
		TypeCutter<E> cutter = getCutter(clazz);
		findAndExecuteSetters(clazz, cutter);
		return cutter.getGeneratedObject();
	}

	private <E> TypeCutter<E> getCutter(Class<E> clazz) {
		Map<String, Field> fieldCache = createFieldCache(clazz);
		TypeCutter<E> cutter = null;
		try {
			cutter = new TypeCutter<E>(clazz.newInstance(), fieldCache);
		} catch (Exception ex) {
			throw new RuntimeException("Cannot instantiate cutter.", ex);
		}
		return cutter;
	}

	private <E> void findAndExecuteSetters(Class<E> clazz, TypeCutter<E> cutter)
			throws SecurityException {
		for (Method m : clazz.getMethods()) {
			if (m.getName().startsWith("set")) {
				cutter.checkArguments(m);
			}
		}
	}

	private <E> Map<String, Field> createFieldCache(Class<E> clazz) {
		Map<String, Field> fieldCache = new HashMap<>();
		Field[] fields = clazz.getDeclaredFields();

		for (Field f : fields) {
			fieldCache.put(f.getName().toLowerCase(), f);
		}
		return fieldCache;
	}
}
