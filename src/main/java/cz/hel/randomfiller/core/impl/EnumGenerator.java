package cz.hel.randomfiller.core.impl;

import java.util.Random;

import cz.hel.randomfiller.core.TypeGenerator;

/**
 * EnumGenerat does not really generate some new enumeration constant,
 * but picks one of defined constants for given class.
 * @author j
 */
public class EnumGenerator implements TypeGenerator {

	private final Random random = new Random();

	@Override
	public Object getGeneratedObject(Class<?> clazz) {
		if (!clazz.isEnum()) {
			throw new IllegalArgumentException("Enum generator class has to be an enum.");
		}

		Object[] enumConstants = clazz.getEnumConstants();
		int randomConstantIndex = random.nextInt(enumConstants.length);
		
		return enumConstants[randomConstantIndex];
	}

}
