package cz.hel.randomfiller.core.impl;

import java.util.Random;
import java.util.UUID;

import cz.hel.randomfiller.core.TypeGenerator;

/**
 *
 * @author j
 */
public class PrimitiveGenerator implements TypeGenerator {

	// TODO chelo by nacist z externiho souboru
	// a vubec tohle odprimitivnit
	protected int intValue;
	protected boolean booleanValue;
	protected long longValue;
	protected float floatValue;
	protected double doubleValue;
	protected short shortValue;
	protected char charValue;
	protected byte[] byteValue = new byte[100];
	protected String stringValue;
	protected Random random = new Random();

	public PrimitiveGenerator() {
		this.intValue = random.nextInt();
		this.shortValue = (short) random.nextInt(Short.MAX_VALUE);
		this.booleanValue = random.nextBoolean();
		this.longValue = random.nextLong();
		this.floatValue = random.nextFloat();
		this.doubleValue = random.nextDouble();
		this.charValue = (char) random.nextInt(255);
		this.stringValue = UUID.randomUUID().toString();
	}

	@Override
	public Object getGeneratedObject(Class<?> clazz) {
		return getPrimitiveForClass(clazz);
	}

	// plus plus srandicky, fujky
	protected Object getPrimitiveForClass(Class<?> clazz) {
		if (clazz.equals(Short.TYPE) || clazz.equals(Short.class)) {
			return ++shortValue;
		} else if (clazz.equals(Integer.TYPE) || clazz.equals(Integer.class)) {
			return ++intValue;
		} else if (clazz.equals(Double.TYPE) || clazz.equals(Double.class)) {
			return ++doubleValue;
		} else if (clazz.equals(Float.TYPE) || clazz.equals(Float.class)) {
			return ++floatValue;
		} else if (clazz.equals(Long.TYPE) || clazz.equals(Long.class)) {
			return ++longValue;
		} else if (clazz.equals(Boolean.TYPE) || clazz.equals(Boolean.class)) {
			random.nextBytes(byteValue);
			return booleanValue;
		} else if (clazz.equals(Byte.TYPE) || clazz.equals(Byte.class)) {
			return byteValue;
		} else if (clazz.equals(Character.TYPE)
				|| clazz.equals(Character.class)) {
			return ++charValue;
		} else { // if (clazz.equals(String.class))
			return stringValue;
		}
	}
}
