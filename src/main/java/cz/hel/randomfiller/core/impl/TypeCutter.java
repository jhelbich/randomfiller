package cz.hel.randomfiller.core.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.hel.randomfiller.core.TypeGenerator;

/**
 *
 * @author j
 */
public class TypeCutter<E> {

	private static final Logger logger = LoggerFactory.getLogger(TypeCutter.class);

	private E generatedObject;

	// TODO singletons
	private PrimitiveGenerator primitives;
	private ComplexGenerator complexOnes;
	private CollectionGenerator collections;
	private Map<String, Field> fieldCache;
	private final AnnotationConstraintsManager constraints = AnnotationConstraintsManager.getInstance();

	public TypeCutter(E generatedObject, Map<String, Field> fieldCache) {
		this.generatedObject = generatedObject;
		this.fieldCache = fieldCache;
	}

	public void checkArguments(Method m) {
		// for setters it is actually only one parameter
		for (Class<?> p : m.getParameterTypes()) {
			// primitives
			if (isPrimitive(p)) {
				Object generatedPrimitive = getPrimitiveGenerator().getGeneratedObject(p);
				Annotation[] fieldAnnotations = getPropertyMetadata(m);
				
				// TODO replace with context
				constraints.addConstraintVar(fieldAnnotations);
				generatedPrimitive = constraints.handleObjectConstraints(generatedPrimitive);
				invokeSetter(generatedPrimitive, m);
			}
			
			// collections
			else if (Collection.class.isAssignableFrom(p)) {
				ParameterizedType t = (ParameterizedType) m.getGenericParameterTypes()[0];
				for (Type z : t.getActualTypeArguments()) {
					Class<?> c = (Class<?>) z;
					TypeGenerator tg = getCollectionsGenerator();
					invokeSetter(tg.getGeneratedObject(c), m);
				}
			}
			// complex types
			else {
				TypeGenerator cg = getComplexGenerator();
				invokeSetter(cg.getGeneratedObject(p), m);
			}
		}
	}

	public E getGeneratedObject() {
		return generatedObject;
	}

	/**
	 * Invokes the setter method with generated value as parameter. Therefore
	 * set its value in the result object.
	 * 
	 * @param argument
	 *            generated value
	 * @param m
	 *            setter method
	 */
	private void invokeSetter(Object argument, Method m) {
		try {
			m.invoke(generatedObject, argument);
		} catch (Exception e) {
			logger.error("Exception while invoking setter method.", e);
		}
	}

	private TypeGenerator getPrimitiveGenerator() {
		if (primitives == null) {
			primitives = new PrimitiveGenerator();
		}
		return primitives;
	}

	private TypeGenerator getComplexGenerator() {
		if (complexOnes == null) {
			complexOnes = new ComplexGenerator();
		}
		return complexOnes;
	}

	private TypeGenerator getCollectionsGenerator() {
		if (collections == null) {
			collections = new CollectionGenerator();
		}
		return collections;
	}

	private boolean isPrimitive(Class<?> p) {
		return p.isPrimitive() || p.getPackage() == null
				|| p.getPackage().getName().startsWith("java.lang");
	}

	// TODO getter meta-data
	/**
	 * Returns meta-data for provided field.
	 * 
	 * @param m
	 *            setter method
	 * @return array of Annotations
	 */
	private Annotation[] getPropertyMetadata(Method m) {
		String[] split = m.getName().split("set");
		String fieldName = "";
		for (int i = 1; i < split.length; ++i) {
			fieldName += split[i];
		}
		Field field = fieldCache.get(fieldName.toLowerCase());
		if (field == null) {
			return null;
		}
		Annotation[] fieldAnnotations = field.getDeclaredAnnotations();
		return fieldAnnotations;
	}
}
