package cz.hel.randomfiller.core.impl.primitive;

/**
 *
 * @author j
 */
public interface ConstraintHandler {

	public Object generateValue(Object actual, Object param);
}
