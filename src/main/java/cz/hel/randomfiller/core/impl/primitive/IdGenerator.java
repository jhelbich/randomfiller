package cz.hel.randomfiller.core.impl.primitive;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author j
 */
public class IdGenerator implements ConstraintHandler {

	private Random random = new Random();
	private Set<Object> usedIds = new HashSet<>();

	@Override
	public Object generateValue(Object actual, Object param) {
		if (usedIds.contains(actual)) {
			Class<? extends Object> clazz = actual.getClass();
			if (clazz.equals(String.class)) {
				
				// TODO temp workaround!
				String curr = (String) actual;
				String nextId = null;
				// TODO not safe!
				while (usedIds.contains((nextId = generateNextString(curr.length()))))
					;
				usedIds.add(nextId);
				return nextId;
			} else if (clazz.equals(Long.class)) {
				Long nextId = null;
				// TODO not safe!
				while (usedIds.contains((nextId = generateNextLong())))
					;
				usedIds.add(nextId);
				return nextId;
			} else if (clazz.equals(Integer.class)) {
				Integer nextId = null;
				// TODO not safe!
				while (usedIds.contains((nextId = generateNextInteger())))
					;
				usedIds.add(nextId);
				return nextId;
			}
			// TODO
			else {
				return null;
			}
		}
		usedIds.add(actual);
		return actual;
	}

	private String generateNextString() {
		String nextId = UUID.randomUUID().toString();
		return nextId;
	}
	
	private String generateNextString(int length) {
		String nextId = UUID.randomUUID().toString();
		nextId = nextId.substring(0, length);
		return nextId;
	}

	private Long generateNextLong() {
		return random.nextLong();
	}

	private int generateNextInteger() {
		return random.nextInt();
	}
}
