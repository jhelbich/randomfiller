package cz.hel.randomfiller.core.impl.primitive;

/**
 *
 * @author j
 */
public class LengthGenerator implements ConstraintHandler {

	@Override
	public Object generateValue(Object actual, Object param) {
		if (param == null) {
			throw new IllegalArgumentException(
					"Length generator's param cannot be null.");
		}
		if (actual.getClass().equals(String.class)) {
			String value = (String) actual;
			Integer length = (Integer) param;
			if (length < 0) {
				throw new IllegalArgumentException(
						"Length generator's param must be >= 0.");
			}
			if (value.length() > length) {
				return value.substring(0, length);
			}
		}
		return actual;
	}
}
