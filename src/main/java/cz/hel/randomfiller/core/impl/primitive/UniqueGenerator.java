package cz.hel.randomfiller.core.impl.primitive;

import java.util.HashSet;
import java.util.Set;

import cz.hel.randomfiller.core.impl.PrimitiveGenerator;

/**
 *
 * @author j
 */
public class UniqueGenerator implements ConstraintHandler {

	private PrimitiveGenerator pg = new PrimitiveGenerator();
	private Set<Object> uniques = new HashSet<>();

	@Override
	public Object generateValue(Object actual, Object param) {
		if (uniques.contains(actual)) {
			while (uniques.contains((actual = pg.getGeneratedObject(actual.getClass()))))
				;
		}
		uniques.add(actual);
		return actual;
	}
}
