package cz.hel.randomfiller.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import cz.hel.randomfiller.Filler;
import cz.hel.randomfiller.GenerationRule;
import cz.hel.randomfiller.ObjectFiller;
import cz.hel.randomfiller.exampleclasses.TestEntity;
import cz.hel.randomfiller.exampleclasses.TestingClass;

/**
 *
 * @author j
 */
public class ObjectFillerTest {

	@Test
	public void testObjectFillerPOJO() {
		Filler filler = new ObjectFiller();
		TestingClass dummy = filler.fillDummy(TestingClass.class);

		assertNotNull(dummy);
		assertNotNull(dummy.getString());
		assertNotNull(dummy.getCompositeClass());
		assertNotNull(dummy.getCompositeClassesList());
		assertNotNull(dummy.getStringList());
		assertEquals(1, dummy.getCompositeClassesList().size());
		assertEquals(1, dummy.getStringList().size());
	}

	@Test
	public void testObjectFillerEntity() {
		int length = 5;
		Filler filler = new ObjectFiller();
		TestEntity dummy = filler.fillDummy(TestEntity.class);

		assertNotNull(dummy);
		assertNotNull(dummy.getCol());
		assertEquals(length, dummy.getCol().length());

		TestEntity dummy2 = filler.fillDummy(TestEntity.class);

		assertNotNull(dummy2);
		assertNotNull(dummy2.getCol());
		assertEquals(length, dummy2.getCol().length());
		assertNotEquals(dummy.getCol(), dummy2.getCol());
	}

	@Test
	public void testGeneratePrimitiveCollection() {
		Filler filler = new ObjectFiller();
		TestingClass dummy = filler.fillDummy(TestingClass.class);

		assertNotNull(dummy);
		assertNotNull(dummy.getString());
		assertNotNull(dummy.getCompositeClass());
		assertNotNull(dummy.getStringList());
		assertEquals(1, dummy.getStringList().size());
	}

	@Test
	public void testGenerateComplexCollection() {
		Filler filler = new ObjectFiller();
		TestingClass dummy = filler.fillDummy(TestingClass.class);

		assertNotNull(dummy);
		assertNotNull(dummy.getString());
		assertNotNull(dummy.getCompositeClass());
		assertNotNull(dummy.getCompositeClassesList());
		assertEquals(1, dummy.getCompositeClassesList().size());
		assertEquals(1, dummy.getCompositeClassesList().size());
		// Demeterka breci, muhehe
		assertEquals(1, dummy.getCompositeClassesList().get(0).getStrings()
				.size());
	}

	@Test
	public void testGenerateWithoutCollections() {
		Filler filler = new ObjectFiller();
		TestingClass dummy = filler.fillDummy(TestingClass.class,
				new GenerationRule[] { GenerationRule.NO_COLLECTIONS });

		assertNotNull(dummy);
		assertNotNull(dummy.getString());
		assertNotNull(dummy.getCompositeClass());
		assertEquals(0, dummy.getStringList().size());
		assertEquals(0, dummy.getCompositeClassesList().size());
		assertEquals(0, dummy.getCompositeClass().getStrings().size());
	}
}
