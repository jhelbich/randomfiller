package cz.hel.randomfiller.core.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.lang.reflect.Field;

import javax.persistence.Column;

import org.junit.Before;
import org.junit.Test;

import cz.hel.randomfiller.exampleclasses.TestEntity;

/**
 *
 * @author j
 */
public class AnnotationConstraintsManagerTest {

	private AnnotationConstraintsManager manager = AnnotationConstraintsManager
			.getInstance();

	@Before
	public void before() {
		Class<TestEntity> c = TestEntity.class;
		for (Field f : c.getDeclaredFields()) {
			manager.addConstraintVar(f.getDeclaredAnnotations());
		}
	}

	@Test
	public void testAddConstraintVar() {
		// 1 je za Id - to nema field, ktereho bych se chytnul
		// staci test na velikost - je to mapa, duplikace nejsou povoleny
		assertEquals(1 + Column.class.getDeclaredMethods().length, manager
				.getConstraints().size());
	}

	/**
	 * TestEntity ma jeden field s anotacemi Id a Column(len = 5).
	 */
	@Test
	public void testHandleObjectConstraints() {
		int length = 5;
		String res = (String) manager
				.handleObjectConstraints("ls kjdflkjaslfkjsdfj");

		assertEquals(length, res.length());
		assertEquals("ls kj", res);

		String res2 = (String) manager
				.handleObjectConstraints("ls kjdflkjaslfkjsdfj");
		assertEquals(length, res2.length());
		assertNotEquals("ls kj", res2);
		assertNotEquals(res, res2);
	}
}