package cz.hel.randomfiller.core.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import cz.hel.randomfiller.GenerationRule;
import cz.hel.randomfiller.core.cache.GenerationRulesCache;
import cz.hel.randomfiller.exampleclasses.PrimitiveTestClass;

/**
 * Pokryje vsechen obsah podminek - DC.
 * 
 * @author j
 */
public class CollectionGeneratorTest {

	@Test
	public void testGeneratedWrapperCollection() {
		CollectionGenerator cg = new CollectionGenerator();
		Object strings = cg.getGeneratedObject(String.class);
		assertNotNull(strings);
		assertEquals(1, ((List<?>) strings).size());
	}

	@Test
	public void testGeneratedComplexCollection() {
		CollectionGenerator cg = new CollectionGenerator();
		Object testClassList = cg.getGeneratedObject(PrimitiveTestClass.class);
		assertNotNull(testClassList);
		assertEquals(1, ((List<?>) testClassList).size());
	}

	@Test
	public void testGenerateNoCollections() {
		GenerationRulesCache.getInstance().createRulesCache(
				new GenerationRule[] { GenerationRule.NO_COLLECTIONS });
		CollectionGenerator cg = new CollectionGenerator();
		Object testClassList = cg.getGeneratedObject(PrimitiveTestClass.class);
		assertNotNull(testClassList);
		assertEquals(0, ((List<?>) testClassList).size());
	}

	@Test
	public void testPrimitiveGeneration() {
		CollectionGenerator cg = new CollectionGenerator();
		int val = 666;
		Object toput = val; // nevim jak je to s autoboxingem
		Object result = cg.getGeneratedObject(toput.getClass());
		assertNotNull(result);
		assertEquals(1, ((List<?>) result).size());
		assertEquals(Integer.class, ((List<?>) result).get(0).getClass());
	}

}
