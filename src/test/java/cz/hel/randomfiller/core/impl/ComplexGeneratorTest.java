package cz.hel.randomfiller.core.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import cz.hel.randomfiller.Filler;
import cz.hel.randomfiller.GenerationRule;
import cz.hel.randomfiller.ObjectFiller;
import cz.hel.randomfiller.exampleclasses.TestingClass;

/**
 *
 * @author j
 */
public class ComplexGeneratorTest {

	@Test
	public void testGenerateNoComplexTypes() {
		Filler filler = new ObjectFiller();
		TestingClass dummy = filler.fillDummy(TestingClass.class,
				new GenerationRule[] { GenerationRule.NO_COPLEX_TYPES });

		assertNotNull(dummy);
		assertNotNull(dummy.getString());
		assertNull(dummy.getCompositeClass());
		assertNotNull(dummy.getCompositeClassesList());
		assertNotNull(dummy.getStringList());
		assertEquals(1, dummy.getCompositeClassesList().size());
		assertEquals(1, dummy.getStringList().size());
		assertFalse(dummy.getInteger() == 0);
	}

	@Test
	public void testGenerateOnlySimpleTypes() {
		Filler filler = new ObjectFiller();
		TestingClass dummy = filler.fillDummy(TestingClass.class,
				new GenerationRule[] { GenerationRule.NO_COPLEX_TYPES,
						GenerationRule.NO_COLLECTIONS });

		assertNotNull(dummy);
		assertNotNull(dummy.getString());
		assertNull(dummy.getCompositeClass());
		assertNotNull(dummy.getCompositeClassesList());
		assertNotNull(dummy.getStringList());
		assertEquals(0, dummy.getCompositeClassesList().size());
		assertEquals(0, dummy.getStringList().size());
		assertFalse(dummy.getInteger() == 0);
	}
}
