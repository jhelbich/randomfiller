package cz.hel.randomfiller.core.impl;

import java.lang.reflect.Field;

public class EnumGeneratorTest {

	private HelloEnum he = HelloEnum.AHOJ;
	
	public enum HelloEnum {
		AHOJ, CAU, DOBRY_DEN;
	}
	
	public static void main(String[] args) {
		EnumGeneratorTest egt = new EnumGeneratorTest();
		for (Field field : egt.getClass().getDeclaredFields()) {
			
			System.out.println("name : " + field.getName() + ", type : " + field.getType());
			System.out.println("is enum : " + field.getType().isEnum());
			
			StringBuilder sb = new StringBuilder();
			for (Object enumConstant : field.getType().getEnumConstants()) {
				sb.append(enumConstant).append(", ");
			}
			sb.delete(sb.length() - 2, sb.length());
			
			System.out.println("enum constants : " + sb.toString());
			
		}
	}
}
