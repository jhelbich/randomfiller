package cz.hel.randomfiller.core.impl;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import cz.hel.randomfiller.Filler;
import cz.hel.randomfiller.ObjectFiller;
import cz.hel.randomfiller.exampleclasses.PrimitiveTestClass;

/**
 *
 * @author j
 */
public class PrimitiveGeneratorTest {

	public PrimitiveGeneratorTest() {
	}

	// systemovy test pro generovani vsech primitivnich typu
	@Test
	public void testOverallPrimitiveRandomGeneration() {
		Filler filler = new ObjectFiller();
		// heavy, lol
		PrimitiveTestClass dummy1 = filler.fillDummy(PrimitiveTestClass.class);
		PrimitiveTestClass dummy2 = filler.fillDummy(PrimitiveTestClass.class);

		assertNotEquals(dummy1.getInteger(), dummy2.getInteger());
		assertNotEquals(dummy1.getShortValue(), dummy2.getShortValue());
		assertNotEquals(dummy1.getFloatValue(), dummy2.getFloatValue());
		assertNotEquals(dummy1.getDoubleValue(), dummy2.getDoubleValue());
		assertNotEquals(dummy1.getString(), dummy2.getString());
		// generuje se to na <0;256) z ASCII, to nema cenu testovat,
		// 1 ku 256 je az moc dobra sance, ze to spadne
		// assertNotEquals(dummy1.getCharacter(), dummy2.getCharacter());
	}

	/**
	 * Unit pro par testovacich typu. MCC.
	 */
	@Test
	public void testGetPrimitiveForClass() {
		PrimitiveGenerator pg = new PrimitiveGenerator();
		Object intval = pg.getPrimitiveForClass(Integer.class);
		Object shortval = pg.getPrimitiveForClass(Short.class);
		Object doubleval = pg.getPrimitiveForClass(Double.class);
		Object longval = pg.getPrimitiveForClass(Long.class);
		Object byteval = pg.getPrimitiveForClass(Byte.class);
		Object charval = pg.getPrimitiveForClass(Character.class);
		Object floatval = pg.getPrimitiveForClass(Float.class);
		Object stringval = pg.getPrimitiveForClass(String.class);

		assertNotNull(intval);
		assertNotNull(doubleval);
		assertNotNull(shortval);
		assertNotNull(longval);
		assertNotNull(byteval);
		assertNotNull(charval);
		assertNotNull(floatval);
		assertNotNull(stringval);
	}

}
