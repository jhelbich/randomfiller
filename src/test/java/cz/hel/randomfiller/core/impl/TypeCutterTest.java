package cz.hel.randomfiller.core.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import cz.hel.randomfiller.core.cache.GenerationRulesCache;
import cz.hel.randomfiller.exampleclasses.TestingClass;
import cz.hel.randomfiller.exampleclasses.TestingCompositeClass;

/**
 *
 * @author j
 */
public class TypeCutterTest {

	/**
	 * Test of checkArguments method, of class TypeCutter.
	 */
	@Test
	public void testCheckArguments() throws Exception {
		GenerationRulesCache rulesCache = GenerationRulesCache.getInstance();
		rulesCache.createRulesCache(null);
		Map<String, Field> fieldCache = new HashMap<>();
		TestingClass tc = new TestingClass();
		Field[] fields = TestingClass.class.getDeclaredFields();

		for (Field f : fields) {
			fieldCache.put(f.getName().toLowerCase(), f);
		}
		TypeCutter<TestingClass> cutter = new TypeCutter<>(tc, fieldCache);

		assertEquals(0, tc.getInteger());
		Method setInt = tc.getClass().getDeclaredMethod("setInteger",
				Integer.class);
		cutter.checkArguments(setInt);
		assertNotEquals(0, tc.getInteger());

		assertEquals(null, tc.getString());
		Method setString = tc.getClass().getDeclaredMethod("setString",
				String.class);
		cutter.checkArguments(setString);
		assertNotNull(tc.getString());
		assertNotEquals("", tc.getString());

		assertEquals(null, tc.getCompositeClass());
		Method setCompositeClass = tc.getClass().getDeclaredMethod(
				"setCompositeClass", TestingCompositeClass.class);
		cutter.checkArguments(setCompositeClass);
		assertNotNull(tc.getCompositeClass());

		assertEquals(null, tc.getStringList());
		Method setStringList = tc.getClass().getDeclaredMethod("setStringList",
				List.class);
		cutter.checkArguments(setStringList);
		assertNotNull(tc.getStringList());
		assertEquals(1, tc.getStringList().size());

		assertEquals(null, tc.getCompositeClassesList());
		Method setCompositeClassesList = tc.getClass().getDeclaredMethod(
				"setCompositeClassesList", List.class);
		cutter.checkArguments(setCompositeClassesList);
		assertNotNull(tc.getCompositeClassesList());
		assertEquals(1, tc.getCompositeClassesList().size());

	}

}