package cz.hel.randomfiller.core.impl.primitive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import cz.hel.randomfiller.core.impl.primitive.IdGenerator;

/**
 *
 * @author j
 */
public class IdGeneratorTest {

	/**
	 * Testuju tri datove typy Id.
	 */
	@Test
	public void testGenerateValue() {
		IdGenerator instance = new IdGenerator();
		String actualString = "generateValue";
		Object result = instance.generateValue(actualString, null);
		assertEquals(actualString, result);

		Long actualLong = 234234L;
		Object result2 = instance.generateValue(actualLong, null);
		assertEquals(actualLong, result2);

		int actualInt = 234243;
		Object result3 = instance.generateValue(actualInt, null);
		assertEquals(actualInt, result3);
	}

	/**
	 * Testuju opakovany pokus o generovani stejneho Stringu pro Id.
	 */
	@Test
	public void testGenerateUniqueStringId() {
		IdGenerator instance = new IdGenerator();
		Set<Object> results = new HashSet<>();
		String actualString = "generateValue";
		Object result = instance.generateValue(actualString, null);
		assertEquals(actualString, result);
		results.add(result);

		for (int i = 0; i < 1000; ++i) {
			// uz to tam jednou je, musi vratit unikatni token
			result = instance.generateValue(result, null);
			assertFalse(results.contains(result));
			results.add(result);
		}
	}

	/**
	 * Testuju opakovany pokus o generovani stejneho Longu pro Id.
	 */
	@Test
	public void testGenerateUniqueLongId() {
		IdGenerator instance = new IdGenerator();
		Set<Object> results = new HashSet<>();
		Long actualLong = 234234L;
		Object result = instance.generateValue(actualLong, null);
		assertEquals(actualLong, result);
		results.add(result);

		for (int i = 0; i < 1000; ++i) {
			// uz to tam jednou je, musi vratit unikatni token
			result = instance.generateValue(result, null);
			assertFalse(results.contains(result));
			results.add(result);
		}
	}

	/**
	 * Testuju opakovany pokus o generovani stejneho Integeru pro Id.
	 */
	@Test
	public void testGenerateUniqueIntegerId() {
		IdGenerator instance = new IdGenerator();
		Set<Object> results = new HashSet<>();
		int actualInt = 234243;
		Object result = instance.generateValue(actualInt, null);
		assertEquals(actualInt, result);
		results.add(result);

		for (int i = 0; i < 1000; ++i) {
			// uz to tam jednou je, musi vratit unikatni token
			result = instance.generateValue(result, null);
			assertFalse(results.contains(result));
			results.add(result);
		}
	}
}