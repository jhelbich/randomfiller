package cz.hel.randomfiller.core.impl.primitive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import cz.hel.randomfiller.core.impl.primitive.LengthGenerator;

/**
 * Pairwise testing. Mezni podminka je length > 0;
 * 
 * @author j
 */
public class LengthGeneratorTest {

	/**
	 * Orizni generovanou hodnotu na delku 3.
	 */
	@Test
	public void testGenerateValue() {
		String actual = "ahoj jak se mas";
		int param = 3;
		LengthGenerator instance = new LengthGenerator();
		Object result = instance.generateValue(actual, param);
		assertEquals("aho", result);
	}

	@Test
	public void testGenerateValueWithBiggerParam() {
		String actual = "ahoj jak se mas";
		int param = 300;
		LengthGenerator instance = new LengthGenerator();
		Object result = instance.generateValue(actual, param);
		assertEquals(actual, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGenerateValueWithFaulParam() {
		String actual = "ahoj jak se mas";
		int param = -13;
		LengthGenerator instance = new LengthGenerator();
		// should throw exception
		instance.generateValue(actual, param);
		
		fail();
	}
}