package cz.hel.randomfiller.core.impl.primitive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import cz.hel.randomfiller.core.impl.primitive.UniqueGenerator;

/**
 *
 * @author j
 */
public class UniqueGeneratorTest {

	@Test
	public void testGenerateShortValue() {
		UniqueGenerator generator = new UniqueGenerator();
		Object result = generator.generateValue(new Short("5432"), null);
		assertEquals((short) 5432, result);

		result = generator.generateValue(new Short("5432"), null);
		assertNotEquals((short) 5432, result);
		assertEquals(Short.class, result.getClass());
	}

	@Test
	public void testGenerateStringValue() {
		UniqueGenerator generator = new UniqueGenerator();
		String val = "5432";
		Object result = generator.generateValue(val, null);
		assertEquals(val, result);

		result = generator.generateValue(val, null);
		assertNotEquals(val, result);
		assertEquals(String.class, result.getClass());
	}
}