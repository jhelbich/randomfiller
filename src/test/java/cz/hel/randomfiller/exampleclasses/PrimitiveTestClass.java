package cz.hel.randomfiller.exampleclasses;

public class PrimitiveTestClass {

	private int integer;
	private float floatValue;
	private double doubleValue;
	private short shortValue;
	private char character;
	private String string;

	public int getInteger() {
		return integer;
	}

	public void setInteger(int integer) {
		this.integer = integer;
	}

	public float getFloatValue() {
		return floatValue;
	}

	public void setFloatValue(float floatValue) {
		this.floatValue = floatValue;
	}

	public double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public short getShortValue() {
		return shortValue;
	}

	public void setShortValue(short shortValue) {
		this.shortValue = shortValue;
	}

	public char getCharacter() {
		return character;
	}

	public void setCharacter(char character) {
		this.character = character;
	}

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}
}