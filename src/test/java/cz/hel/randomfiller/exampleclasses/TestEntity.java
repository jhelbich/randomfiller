package cz.hel.randomfiller.exampleclasses;

import javax.persistence.Column;
import javax.persistence.Id;

public class TestEntity {

	@Id
	@Column(name = "col", insertable = true, length = 5, unique = false)
	private String col;

	public String getCol() {
		return col;
	}

	public void setCol(String col) {
		this.col = col;
	}
}