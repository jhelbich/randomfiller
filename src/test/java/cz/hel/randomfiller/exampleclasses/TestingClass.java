package cz.hel.randomfiller.exampleclasses;

import java.util.List;

/**
 * Nemenit nazvy metod, je na nich zavisly naprosto zbytecny TypeCutterTest.
 * 
 * @author j
 */
public class TestingClass {

	private int integer;
	private String string;
	private TestingCompositeClass compositeClass;
	private List<String> stringList;
	private List<TestingCompositeClass> compositeClassesList;

	public int getInteger() {
		return integer;
	}

	public void setInteger(Integer integer) {
		this.integer = integer;
	}

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	public TestingCompositeClass getCompositeClass() {
		return compositeClass;
	}

	public void setCompositeClass(TestingCompositeClass compositeClass) {
		this.compositeClass = compositeClass;
	}

	public List<String> getStringList() {
		return stringList;
	}

	public void setStringList(List<String> strings) {
		this.stringList = strings;
	}

	public List<TestingCompositeClass> getCompositeClassesList() {
		return compositeClassesList;
	}

	public void setCompositeClassesList(List<TestingCompositeClass> compositeClassesList) {
		this.compositeClassesList = compositeClassesList;
	}

}
