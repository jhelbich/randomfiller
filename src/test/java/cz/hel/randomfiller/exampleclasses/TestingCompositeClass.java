package cz.hel.randomfiller.exampleclasses;

import java.util.List;

/**
 *
 * @author j
 */
public class TestingCompositeClass {

	private int integer = 666;
	private String string = "Generated string";
	private List<String> strings;

	public List<String> getStrings() {
		return strings;
	}

	public void setStrings(List<String> strings) {
		this.strings = strings;
	}

	public int getInteger() {
		return integer;
	}

	public String getString() {
		return string;
	}

}
